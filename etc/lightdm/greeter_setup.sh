#!/bin/bash
# rotate display-port monitor 90deg counterclockwise
xrandr --output DP-0 --rotate left
# enable numlock
/usr/bin/numlockx on
exit 0
