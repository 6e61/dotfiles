# ArchLinux
Installation is done on **standalone hard drive** without touching already installed system if exists (ie. Windows10). This requires physical removal of already installed system hard drive, or disable it totally in BIOS for the installation time.
Note that for uefi this requires to create **additional** EFI partition on target drive.
Ability to boot other system will be added at the end of successful installation.

> If your are planning dual boot with Windows consider that by default Windows uses local time instead of UTC time. Fix it.[^1]

# BOOT
By default installer boots in native screen resolution. To boot it in lower resolution making the console easier to see edit boot menu parameters.
- at the Arch Boot Menu, hit `e` at the menu to edit parameters
- add `nomodeset video=1280x760` to the list of commands

# INSTALL SETUP[^2]
## prerequisites
- setup keyboard layout `loadkeys pl` *pl=polish programmer keyboard, choose the one fit's you*
- verify the boot mode `ls /sys/firmware/efi/efivars` - if directory exists then system is booted into uefi mode, if no - bios mode
- verify network connection/ip address `ip a`
- if necessery configure wifi connection using iwd[^3] `iwctl`
```
[iwd] device list                   # to show device name
[iwd] station wlan0 connect SSID    # device name replaced with wlan0
```
- check internet connectivity `ping -c2 google.com`
- update sys clock `timedatectl set-ntp true`
- set root password for archiso `passwd`

## *ssh remote access (optional)*
*After steps above you can start sshd (included in the archiso) and finish the installation process from another computer. This enables to have access to copy and paste, editors, browsers and other stuff. Thus below steps in this section are optional.*

- *enable sshd `systemctl start sshd`*
- *connect from **another** machine `ssh root@TARGET_IP_ADDRESS`*

## partition the disks[^4]
- list devices `lsblk -f`
  - go to: [bios/gpt](#bios-with-gpt)
  - go to: [uefi/gpt](#uefi-with-gtp)
  - go to: [uefi/gpt/disk encryption](#uefi-with-gpt-and-disk-encryption)

## bios with gpt
- create new empty GUID partition table (GPT) starting `gdisk /dev/sdX` and in gdisk hit `o`
- create partitions in gdisk `n`

| no | size | type | note | mount point |
| --- | --- | --- | --- | --- |
| 1 | +1M | ef02 | BIOS boot partition |  |
| 2 | +300M | 8300 | linux filesystem | /boot |
| 3 | +16G | 8200 | *linux swap\** |  |
| 4 | +64G | 8304 | linux x86-64 root | / |
| 5 | + | 8302 | *linux /home\** | /home |
*\* optional*

### format disks
- format /boot partition `mkfs.ext4 /dev/sda2`
- format /root partition `mkfs.ext4 /dev/sda4`
- format /home partition `mkfs.ext4 /dev/sda5`
- create swap partition `mkswap /dev/sda3`
- initialize swap partition `swapon /dev/sda3`

### mount
- mount /root `mount /dev/sda4 /mnt`
- create directories for /boot and /home `mkdir -p /mnt/boot`; `mkdir -p /mnt/home`
- mount /boot and /home dirs `mount /dev/sda2 /mnt/boot`; `mount /dev/sda5 /mnt/home`

go to: [install packages](#install-packages)

## uefi with gpt 
- create new empty GUID partition table (GPT) starting `gdisk /dev/sda` and in gdisk hit `o`
- create partitions in gdisk `n`

| no | size | type | note | mount point |
| --- | --- | --- | --- | --- |
| 1 | +512M | ef00 | EFI system partition | /boot/efi |
| 2 | +16G | 8200 | *linux swap\** |  |
| 3 | +64G | 8304 | linux x86-64 root | / |
| 4 | + | 8302 | *linux /home\** | /home |
*\* optional*

### format disks
- format /boot partition `mkfs.fat -F32 /dev/sda1`
- format /root partition `mkfs.ext4 /dev/sda3`
- format /home partition `mkfs.ext4 /dev/sda4`
- create swap partition `mkswap /dev/sda2`
- initialize swap partition `swapon /dev/sda2`

### mount
- mount /root `mount /dev/sda3 /mnt`
- create directories for /boot/efi and /home `mkdir -p /mnt/boot/efi`; `mkdir -p /mnt/home`
- mount /boot and /home dirs `mount /dev/sda1 /mnt/boot/efi`; `mount /dev/sda4 /mnt/home`

go to: [install packages](#install-packages)

## uefi with gpt and disk encryption
- create new empty GUID partition table (GPT) starting `gdisk /dev/sda` and in gdisk hit `o`
- create partitions in gdisk `n`

| no | size | type | note | mount point |
| --- | --- | --- | --- | --- |
| 1 | +512M | ef00 | EFI system partition | /boot/efi |
| 2 | +16G | 8200 | *linux swap\** |  |
| 3 | +64G | 8304 | linux x86-64 root | / |
| 4 | + | 8302 | *linux /home\** | /home |
*\* optional*

### disk encryption[^5]
- encrypt /home partition `cryptsetup -y --use-random luksFormat /dev/sda4`
- open LUKS device `cryptsetup luksOpen /dev/sda3 cryptroot`

### format disks
- format /boot partition `mkfs.fat -F32 /dev/sda1`
- format /root partition `mkfs.ext4 /dev/sda3`
- format /home partition `mkfs.ext4 /dev/mapper/cryptroot`
- create swap partition `mkswap /dev/sda2`
- initialize swap partition `swapon /dev/sda2`

### mount
- mount /root `mount /dev/sda3 /mnt`
- create directories for /boot/efi and /home `mkdir -p /mnt/boot/efi`; `mkdir -p /mnt/home`
- mount /boot/efi and /home dirs `mount /dev/sda1 /mnt/boot/efi`; `mount /dev/mapper/cryptroot`

### initramfs (w/ encryption)
- add encrypt to HOOKS `/etc/mkinitcpio.conf` on encrypted system (**order matters**) `HOOKS=(base udev autodetect keyboard modconf block encrypt filesystems fsck)`

### grub bootloader (w/ encryption)
- determine the UUID of encrypted /root partition `blkid`
- edit GRUB config file `vim /etc/default/grub`
- update the GRUB_CMDLINE_LINUX
```
GRUB_CMDLINE_LINUX="cryptdevice=UUID=TARGET_DRIVE_UUID:cryptroot root=/dev/mapper/cryptroot"
```
go to: [install packages](#install-packages)

## install packages
- install packages on root
`pacstrap /mnt base base-devel linux-lts linux-firmware linux grub nano vim git networkmanager man-db man-pages texinfo dosfstools ntfs-3g exfat-utils usbutils nfs-utils udisks2 udiskie bash-completion openssh mc`
*`intel-ucode` only if intel onboard*
*`efibootmgr` only if using uefi*
- generate fstab file `genfstab -U /mnt >> /mnt/etc/fstab`


# CONFIGURATION
- change root into system `arch-chroot /mnt`
- set timezone `ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime` *Warsaw=polish DST time zone, choose the one fit's you*
- genrate timeadj `hwclock --systohc`

## locale
- uncomment `en_US.UTF-8`, *`pl_PL.UTF-8`* in locale.gen file `vim /etc/locale.gen`
- generate locale `locale-gen`
- set system language variable `echo "LANG=en_US.UTF-8" >> /etc/locale.conf`
- set keyboard layout `echo "KEYMAP=pl" >> /etc/vconsole.conf` *pl=polish programmer keyboard, choose the one fit's you*
- set hostname `echo "hostname" >> /etc/hostname` *hostname=give a fancy name for your pc*
- edit hosts files `vim /etc/hosts`
```
127.0.0.1	localhost
::1		localhost
127.0.1.1	hostname.localdomain	hostname
```

## initramfs (w/o encryption)
- build initramfs for lts kernel `mkinitcpio -P linux-lts`

## grub bootloader (w/o encryption)
- install GRUB `grub-install` if mbr install GRUB `grub-install --target=i386-pc /dev/sda`
- generate GRUB config file `grub-mkconfig -o /boot/grub/grub.cfg`

## user administration[^6]
- create user adding it to wheel group to be able make sudo commands `useradd -m -G wheel username`
- assign user passwrod `passwd username`
- edit sudoers enabling wheel group `visudo /etc/sudoers` uncomment line `%wheel ALL=(ALL) ALL`
- enable network manager to for autoconnection `systemct enable NetworkManager`

## reboot
- exit arch-root `exit`
- umount partitions `umount -R /mnt`
- reboot `shutdown -r now`

**After reboot enter into BIOS (by pressing DEL or F2) to point UEFI manager efi file**

From this point you shall be able to start Arch via GRUB and login into it using previously created account. If so shutdown system.
Insert or enable previously removed/disabled hard drive with oryginal system. 
> Now using UEFI bootmanager of your motherboard (by pressing F12 on keyboard while booting) you are able to choose 2 different hard drive disk with 2 different systems. 
If your Windows installation has Bitlocker ON, then this is only way to boot Windows. If you try to boot Windows from Grub it will asks for Bitlocker Recovery Key. It is due to EFI partition was not loaded by UEFI. This happens because Bitlocker keys are stored in TPM module.

# extra sources
- [Arch Linux and Windows 10 (UEFI + Encrypted) Install Guide](https://octetz.com/docs/2020/2020-2-16-arch-windows-install)
- [Dual boot with Windows](https://wiki.archlinux.org/index.php/Dual_boot_with_Windows)
- [Dual Boot Installation of Arch Linux with Preinstalled Windows 10 with Encryption](http://tech.memoryimprintstudio.com/dual-boot-installation-of-arch-linux-with-preinstalled-windows-10-with-encryption/)
- [Arch Linux Installation and Configuration on UEFI Machines](https://www.tecmint.com/arch-linux-installation-and-configuration-guide/)

[^1]: https://wiki.archlinux.org/index.php/System_time#UTC_in_Windows
[^2]: https://wiki.archlinux.org/index.php/Installation_guide
[^3]: https://wiki.archlinux.org/index.php/Iwd#iwctl
[^4]: https://superuser.com/a/1166518
[^5]: https://octetz.com/docs/2020/2020-2-16-arch-windows-install/
[^6]: https://wiki.archlinux.org/index.php/General_recommendations

