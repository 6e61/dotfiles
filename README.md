# ArchLinux

Quick guide how to install ArchLinux from scratch on bios/mbr bios/gpt uefi/gpt cases, optionally with disk encryption.

## Base installation
[Base ArchLinux installation](docs/01-base-install-arch.md)

## Personalization
Work in progress document: Some personalization [documentation](docs/10-personalization-arch.md) with config [files](home/).

### screnshoots

- **Fonts**: Tahoma + Monospace
- **Display Manager**: lightdm (greeter: lightdm-webkit-theme-aether)
- **Desktop Environment/Window Manager**: xfce4 + i3-gaps + py3status + rofi
- **Theme**: [Arc-Dark](https://github.com/arc-design/arc-theme)
- **Icons**: Arc-ICONS [source](https://github.com/horst3180/arc-icon-theme) + [aur](https://aur.archlinux.org/packages/arc-icon-theme-git)
- **Notification**: [dunst](https://github.com/dunst-project/dunst)
- **Wallpapers**: [primary](http://getwallpapers.com/collection/arch-linux-wallpaper) + [secondary](https://onezero.medium.com/the-dark-forest-theory-of-the-internet-7dc3e68a7cb1)

*py3status: primary display with 3 groups cycled every 5s*

![py3status-2021-primary-group1](docs/img/desktop-2021-03-14-primary-group1.png)

![py3status-2021-primary-group2](docs/img/desktop-2021-03-14-primary-group2.png)

![py3status-2021-primary-group3](docs/img/desktop-2021-03-14-primary-group3.png)

![dunst](docs/img/desktop-2021-03-15-primary-dunst.png)

**py3status: secondary display*

![py3status-2021-secondary](docs/img/desktop-2021-03-14-secondary.png)
